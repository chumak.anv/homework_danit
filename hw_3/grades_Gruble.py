# expected inconsistency
grades = {
    "Student_1": {"grade": 78, "state": "Failed"},
    "Student_2": {"grade": 82, "state": "Passed"},
    "Student_3": {"grade": 97, "state": "Passed"},
    "Student_4": {"grade": 86, "state": "Passed"},
    "Student_5": {"grade": 67, "state": "Passed"},
    "Student_6": {"grade": 75, "state": "Passed"},
}

# expected consistency, threshold 73-78 points
# grades = {
#     "Student_1": {"grade": 84, "state": "Passed"},
#     "Student_2": {"grade": 78, "state": "Passed"},
#     "Student_3": {"grade": 65, "state": "Failed"},
#     "Student_4": {"grade": 90, "state": "Passed"},
#     "Student_5": {"grade": 72, "state": "Failed"},
# }


# my tests

# expected consistency, threshold 83-97 points
# grades = {
#     "Student_1": {"grade": 78, "state": "Failed"},
#     "Student_2": {"grade": 82, "state": "Failed"},
#     "Student_3": {"grade": 97, "state": "Passed"},
#     "Student_4": {"grade": 65, "state": "Failed"},
#     "Student_5": {"grade": 67, "state": "Failed"},
#     "Student_6": {"grade": 75, "state": "Failed"},
# }

# expected consistency, threshold 79-100 points
# grades = {
#     "Student_1": {"grade": 78, "state": "Failed"},
# }


# expected inconsistency
# grades = {
#     "Student_1": {"grade": 78, "state": "Failed"},
#     "Student_2": {"grade": 82, "state": "Passed"},
#     "Student_3": {"grade": 97, "state": "Failed"},
#     "Student_4": {"grade": 86, "state": "Passed"},
#     "Student_5": {"grade": 67, "state": "Passed"},
#     "Student_6": {"grade": 75, "state": "Passed"},
# }
