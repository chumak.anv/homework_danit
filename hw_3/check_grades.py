from grades_Gruble import grades


def compare_grades(passed_grades_to_check, failed_grades_to_check):
    inconsistent_failed_grades = []
    for student_passed in passed_grades_to_check:
        for student_failed in failed_grades_to_check:
            if student_failed["grade"] >= student_passed["grade"]:
                inconsistent_failed_grades.append({"reason": student_passed, "inconsistance": student_failed})
    return inconsistent_failed_grades


students_with_passed_grades = []
students_with_failed_grades = []
passed_grades = [100]
failed_grades = [0]

for student, student_grade in grades.items():
    if student_grade["state"].lower() == "failed":
        students_with_failed_grades.append({"student": student, "grade": student_grade["grade"]})
        failed_grades.append(student_grade["grade"])

    else:
        students_with_passed_grades.append({"student": student, "grade": student_grade["grade"]})
        passed_grades.append(student_grade["grade"])


inconsistent_grades = compare_grades(students_with_passed_grades, students_with_failed_grades)
if inconsistent_grades:
    for student in inconsistent_grades:
        print(
            f"The professor is inconsistent, because {student['reason']['student']} has a ‘Passed’ mark with a grade of {student['reason']['grade']} and {student['inconsistance']['student']} has a 'Failed’ mark with a higher grade of {student['inconsistance']['grade']}."
        )
else:
    print(
        f"The professor is consistent, and the threshold for passing the exam is in the range of {max(failed_grades)+1} - {min(passed_grades)} points."
    )
