first_number = int(input("Enter first number: "))
second_number = int(input("Enter second number: "))
third_number = int(input("Enter third number: "))

num_list = [first_number, second_number, third_number]
num_list.sort()
print(f"{num_list[0]} <= {num_list[1]} <= {num_list[2]}")

# solution with conditions
if first_number < second_number and third_number > second_number:
    print(f"{first_number} < {second_number} < {third_number}")
elif first_number < third_number and second_number > third_number:
    print(f"{first_number} < {third_number} < {second_number}")
elif second_number < first_number and third_number > first_number:
    print(f"{second_number} < {first_number} < {third_number}")
elif second_number < third_number and first_number > third_number:
    print(f"{second_number} < {third_number} < {first_number}")
elif third_number < first_number and second_number > first_number:
    print(f"{third_number} < {first_number} < {second_number}")
elif third_number < second_number and first_number > second_number:
    print(f"{third_number} < {second_number} < {first_number}")
else:
    print("some of numbres are equal")
