# import sys


def check_division_error(func):
    """
    A decorator that checks for division by zero in the wrapped function.

    If a ZeroDivisionError occurs, it catches the error and prints a message.
    Optionally, sys.exit(1) can be used to stop the program after the
    exception.

    Args:
        func (callable): The function to wrap and check for ZeroDivisionError.

    Returns:
        callable: The wrapped function.
    """

    def wrapper(*args, **kwargs):
        try:
            result = func(*args, **kwargs)
        except ZeroDivisionError as e:
            print(e)
            # sys.exit(1) if needed to exit after exception
        else:
            return result

    return wrapper


@check_division_error
def divide(a, b):
    """
    Divide two numbers and return the result.

    Args:
        a (float or int): The dividend.
        b (float or int): The divisor.

    Returns:
        float: The result of the division, or None if a ZeroDivisionError
        occurs.
    """
    return a / b


@check_division_error
def complex_calculation(x, y, z):
    """
    Perform a complex calculation by adding two numbers and dividing the
    result by a third.

    Args:
        x (float or int): The first number.
        y (float or int): The second number.
        z (float or int): The divisor.

    Returns:
        float: The result of the calculation, or None if a ZeroDivisionError
        occurs.
    """
    return (x + y) / z


# Tests
print(divide(10, 2))  # Expected result: 5.0
print(divide(10, 0))  # Expected result: ZeroDivisionError
print(divide(0, 1))  # Expected result: 0.0

print(complex_calculation(5, 3, 1))  # Expected result: 8.0
print(complex_calculation(5, 3, 0))  # Expected result: ZeroDivisionError
