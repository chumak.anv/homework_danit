# import sys


def check_index_error(func):
    """
    Decorator to check for IndexError.

    This decorator wraps the function and catches IndexError exceptions that
    occur when accessing list elements by index. If an error occurs, it prints
    an error message.
    Optionally, sys.exit(1) can be used to stop the program after the
    exception.

    Args:
        func (callable): The function to wrap.

    Returns:
        callable: The wrapped function.
    """

    def wrapper(*args, **kwargs):
        try:
            result = func(*args, **kwargs)
        except IndexError as e:
            print(e)
            # sys.exit(1) # Uncomment this to exit the program on IndexError
        else:
            return result

    return wrapper


@check_index_error
def get_element(lst, idx):
    """
    Retrieves an item from the list by its index.

    Args:
        lst (list): The list to retrieve the item from.
        idx (int): The index of the item.

    Returns:
        object: The item at the specified index.
    """
    return lst[idx]


@check_index_error
def sum_list_items(my_list, index1, index2):
    """
    Sums two items from the list by their indices.

    Args:
        my_list (list): The list to retrieve the items from.
        index1 (int): The index of the first item.
        index2 (int): The index of the second item.

    Returns:
        float or int: The sum of the two items.
    """
    return my_list[index1] + my_list[index2]


# Tests

# Test 1: Get item by correct index
test_list = [10, 20, 30, 40, 50]
print(get_element(test_list, 2))  # Expected result: 30

# Test 2: Try to get an item by an invalid index
print(get_element(test_list, 10))  # Expected result: IndexError message

# Test 3: Sum two items by correct indices
print(sum_list_items(test_list, 1, 3))  # Expected result: 60

# Test 4: Try to sum items with an invalid index
print(sum_list_items(test_list, 1, 10))  # Expected result: IndexError message
