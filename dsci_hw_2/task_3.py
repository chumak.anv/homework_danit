import numpy as np
from PIL import Image
from typing import Tuple


def load_image(filename: str) -> np.ndarray:
    """
    Load an image from a file and convert it to a NumPy array.

    Args:
        filename (str): The path to the image file.

    Returns:
        np.ndarray: A NumPy array representing the image.
    """
    with Image.open(filename) as img:
        img_matrix = np.array(img)
    return img_matrix


def display_image(img_matrix: np.ndarray) -> None:
    """
    Display an image represented by a NumPy array.

    Args:
        img_matrix (np.ndarray): A NumPy array representing the image.

    Returns:
        None
    """
    img = Image.fromarray(img_matrix)
    img.show()


def calculate_channel_statistics(img_matrix: np.ndarray) -> Tuple[int, int, float]:
    """
    Calculate the min, max, and mean pixel values for each color channel (red, green, blue).

    Args:
        img_matrix (np.ndarray): A NumPy array representing the image.

    Returns:
        Tuple[int, int, float]: A tuple containing min, max, and mean values for each channel.
    """
    red_channel = img_matrix[:, :, 0]
    green_channel = img_matrix[:, :, 1]
    blue_channel = img_matrix[:, :, 2]

    min_red, max_red, mean_red = (
        np.min(red_channel),
        np.max(red_channel),
        np.mean(red_channel),
    )
    min_green, max_green, mean_green = (
        np.min(green_channel),
        np.max(green_channel),
        np.mean(green_channel),
    )
    min_blue, max_blue, mean_blue = (
        np.min(blue_channel),
        np.max(blue_channel),
        np.mean(blue_channel),
    )

    return (
        (min_red, max_red, mean_red),
        (min_green, max_green, mean_green),
        (min_blue, max_blue, mean_blue),
    )


def calculate_total_intensity(img_matrix: np.ndarray) -> Tuple[int, int, int, int]:
    """
    Calculate the total intensity of each color channel (red, green, blue) and the overall image.

    Args:
        img_matrix (np.ndarray): A NumPy array representing the image.

    Returns:
        Tuple[int, int, int, int]: A tuple containing the total intensity for the red, green, and blue channels,
                                   and the overall image.
    """
    total_intensity_red = np.sum(img_matrix[:, :, 0])
    total_intensity_green = np.sum(img_matrix[:, :, 1])
    total_intensity_blue = np.sum(img_matrix[:, :, 2])
    total_intensity = np.sum(img_matrix)

    return (
        total_intensity_red,
        total_intensity_green,
        total_intensity_blue,
        total_intensity,
    )


def linear_normalize(
    img_matrix: np.ndarray, new_min: int = 0, new_max: int = 255
) -> np.ndarray:
    """
    Normalize the pixel values of a grayscale image to a new range using linear normalization.

    Args:
        img_matrix (np.ndarray): The 2D NumPy array representing the grayscale image.
        new_min (int): The desired minimum pixel value in the new range.
        new_max (int): The desired maximum pixel value in the new range.

    Returns:
        np.ndarray: The normalized image matrix.
    """
    old_min = np.min(img_matrix)
    old_max = np.max(img_matrix)

    # Check if the image has variance
    if old_max == old_min:
        print("Warning: The image has no variance. Normalization cannot be performed.")
        return np.full_like(img_matrix, new_min)

    # Apply linear normalization
    normalized_img = (img_matrix - old_min) * (new_max - new_min) / (
        old_max - old_min
    ) + new_min

    # Clip to ensure values are within the correct range
    normalized_img = np.clip(normalized_img, new_min, new_max)

    return normalized_img


def main():
    # Load the image
    filename = "./images/khomyak_view.jpg"
    img_matrix = load_image(filename)

    # Display the original image
    display_image(img_matrix)

    # Calculate and print channel statistics
    red_stats, green_stats, blue_stats = calculate_channel_statistics(img_matrix)
    print(
        f"Red Channel - Min: {red_stats[0]}, Max: {red_stats[1]}, Mean: {red_stats[2]}"
    )
    print(
        f"Green Channel - Min: {green_stats[0]}, Max: {green_stats[1]}, Mean: {green_stats[2]}"
    )
    print(
        f"Blue Channel - Min: {blue_stats[0]}, Max: {blue_stats[1]}, Mean: {blue_stats[2]}"
    )

    # Calculate and print total intensities
    (
        total_intensity_red,
        total_intensity_green,
        total_intensity_blue,
        total_intensity,
    ) = calculate_total_intensity(img_matrix)
    print(f"Total Intensity Red Channel: {total_intensity_red}")
    print(f"Total Intensity Green Channel: {total_intensity_green}")
    print(f"Total Intensity Blue Channel: {total_intensity_blue}")
    print(f"Total Intensity of Entire Image: {total_intensity}")

    # Normalize the grayscale image
    normalized_img_matrix = linear_normalize(img_matrix)

    # Rescale the normalized matrix to 0-255 range (necessary because normalization to 0-1 was performed)
    normalized_img_matrix_display = (normalized_img_matrix * 255).astype(np.uint8)

    # Create and show/save the normalized image
    normalized_img = Image.fromarray(normalized_img_matrix_display)
    normalized_img.show()
    normalized_img.save("images/normalized_image.jpg")


if __name__ == "__main__":
    main()
