import numpy as np


def generate_random_arrays(
    size: tuple[int, int], low: int = 1, high: int = 10
) -> tuple[np.ndarray, np.ndarray]:
    """
    Generates two random integer arrays of specified size with elements
    in the range [low, high).

    Args:
        size (tuple[int, int]): The size of the arrays.
        low (int, optional): The lower bound of the random integers
        (inclusive). Defaults to 1.
        high (int, optional): The upper bound of the random integers
        (exclusive). Defaults to 10.

    Returns:
        tuple[np.ndarray, np.ndarray]: Two randomly generated integer arrays.
    """
    array_1 = np.random.randint(low, high, size=size)
    array_2 = np.random.randint(low, high, size=size)
    return array_1, array_2


def perform_operations_on_arrays(
    array_1: np.ndarray, array_2: np.ndarray
) -> dict[str, np.ndarray]:
    """
    Performs arithmetic operations (sum, difference, product, division, power)
    on two arrays.

    Args:
        array_1 (np.ndarray): The first input array.
        array_2 (np.ndarray): The second input array.

    Returns:
        dict[str, np.ndarray]: A dictionary containing the
        results of the operations.
    """
    operations = {
        "sum": array_1 + array_2,
        "difference": array_1 - array_2,
        "product": array_1 * array_2,
        "division": array_1 / array_2,
        "power": array_1**array_2,
    }
    return operations


def print_array_operations(operations: dict[str, np.ndarray]) -> None:
    """
    Prints the results of array operations.

    Args:
        operations (dict[str, np.ndarray]): A dictionary containing the
        results of the operations.
    """
    for operation_name, result_array in operations.items():
        print(f"\n{operation_name.capitalize()}:")
        print(result_array)


if __name__ == "__main__":
    # Generate random 5x5 arrays
    array_first, array_second = generate_random_arrays(size=(5, 5))

    print("First Array:")
    print(array_first)

    print("\nSecond Array:")
    print(array_second)

    # Perform operations
    operations_result = perform_operations_on_arrays(array_first, array_second)

    # Print results
    print_array_operations(operations_result)
