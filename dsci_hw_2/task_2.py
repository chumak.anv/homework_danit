import numpy as np


def create_chess_board(size: int = 8) -> np.ndarray:
    """
    Creates a chessboard pattern (alternating 0s and 1s) in a 2D NumPy array.

    Args:
        size (int, optional): The size of the chessboard (default is 8x8).

    Returns:
        np.ndarray: A 2D NumPy array representing the chessboard.
    """
    return np.array([[(i + j) % 2 for j in range(size)] for i in range(size)])


def get_row(array: np.ndarray, row_index: int) -> np.ndarray:
    """
    Returns a specified row from a 2D array.

    Args:
        array (np.ndarray): The 2D array.
        row_index (int): The index of the row to retrieve.

    Returns:
        np.ndarray: The specified row from the array.
    """
    return array[row_index]


def get_column(array: np.ndarray, col_index: int) -> np.ndarray:
    """
    Returns a specified column from a 2D array.

    Args:
        array (np.ndarray): The 2D array.
        col_index (int): The index of the column to retrieve.

    Returns:
        np.ndarray: The specified column from the array.
    """
    return array[:, col_index]


def get_subarray(array: np.ndarray, rows: int, columns: int) -> np.ndarray:
    """
    Returns a subarray from the top-left corner of the 2D array.

    Args:
        array (np.ndarray): The 2D array.
        rows (int): The number of rows to include in the subarray.
        columns (int): The number of columns to include in the subarray.

    Returns:
        np.ndarray: The resulting subarray.
    """
    return array[:rows, :columns]


if __name__ == "__main__":
    # Create a chessboard
    chess_board = create_chess_board()

    print("\nChess Board:")
    print(chess_board)

    # Get the third row (index 2)
    third_row = get_row(chess_board, 2)
    print("\nThird Row:")
    print(third_row)

    # Get the fifth column (index 4)
    fifth_column = get_column(chess_board, 4)
    print("\nFifth Column:")
    print(fifth_column)

    # Get a subarray with the left upper subarray 3x3
    subarray = get_subarray(chess_board, 3, 3)
    print("\nSubarray (First 3x3 Upper-Left):")
    print(subarray)
