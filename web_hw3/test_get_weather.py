import pytest
from weather_data import get_weather
import os
from dotenv import find_dotenv, load_dotenv


def test_get_weather_integration():
    """
    Integration test for the `get_weather` function to ensure it fetches
    weather data correctly using a real API key.

    Steps:
    1. Load the API key from the .env file.
    2. Call the `get_weather` function with a valid city and API key.
    3. Assert the presence of required fields in the API response.
    4. Verify the correctness of the returned city name and weather details.

    Raises:
        AssertionError: If any expected key or value is missing in the API
        response.
    """
    env_file: str = find_dotenv(".env")
    load_dotenv(env_file)

    API_KEY: str = os.environ.get("API_KEY", "")
    city: str = "Kyiv"

    result: dict = get_weather(city, API_KEY)

    assert "main" in result, "The response is missing the 'main' key"
    assert "temp" in result["main"], "The 'main' key is missing the 'temp' field"
    assert "name" in result, "The response is missing the 'name' key"
    assert result["name"] == "Kyiv", "The city name does not match the expected value"
    assert "weather" in result, "The response is missing the 'weather' key"
    assert len(result["weather"]) > 0, "The 'weather' list is empty"
    assert (
        "description" in result["weather"][0]
    ), "The weather description is missing in the response"
