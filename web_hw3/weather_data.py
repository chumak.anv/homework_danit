import requests
from typing import Dict


def get_weather(city: str, API_KEY: str) -> Dict:
    """
    Fetches current weather data for a given city using the OpenWeatherMap API.

    Args:
        city (str): The name of the city for which to retrieve weather data.
        API_KEY (str): Your OpenWeatherMap API key.

    Returns:
        Dict: A dictionary containing the weather data, including temperature,
        city name, and weather description.

    Raises:
        requests.exceptions.HTTPError: If the API request fails with an HTTP
        error.
    """
    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {"q": city, "appid": API_KEY, "units": "metric"}

    response = requests.get(url, params=params)

    if response.status_code == 200:
        return response.json()
    else:
        response.raise_for_status()
