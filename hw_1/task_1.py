import math

user_name = input("Enter your name: ")
month_salary = int(input("Enter your salary (in dollars): "))
annual_salary = math.floor(month_salary * 12 / 1000)
print(f"{user_name.title()}'s annual salary is {annual_salary} thousands dollars")
