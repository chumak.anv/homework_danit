import time
from typing import Callable, Set

import Fermat_test
import Rabin_Miller_test


def simple_prime_retrieval(range_to_retrieve: int) -> set:
    """
    Retrieves prime numbers using a simple algorithm.

    Args:
        range_to_retrieve (int): The upper limit of the range to retrieve prime numbers.

    Returns:
        set: A set of prime numbers within the specified range.
    """
    prime_numbers = set()
    for candidate in range(2, range_to_retrieve + 1):
        is_prime = True
        for previous_number in range(2, int(candidate**0.5) + 1):
            if candidate % previous_number == 0:
                is_prime = False
                break
        if is_prime:
            prime_numbers.add(candidate)
    return prime_numbers

def sieve_of_Eratosthenes(range_to_retrieve: int) -> set:
    """
    Retrieves prime numbers using the Sieve of Eratosthenes algorithm.

    Args:
        range_to_retrieve (int): The upper limit of the range to retrieve prime numbers.

    Returns:
        set: A set of prime numbers within the specified range.
    """
    prime_numbers = set()
    prime = [True] * (range_to_retrieve + 1)
    prime[0], prime[1] = False, False  # 0 and 1 are not prime numbers

    for i in range(2, int(range_to_retrieve**0.5) + 1):
        if prime[i]:
            for j in range(i * i, range_to_retrieve + 1, i):
                prime[j] = False

    for i in range(range_to_retrieve + 1):
        if prime[i]:
            prime_numbers.add(i)

    return prime_numbers

def Fermat_test_for_range(range_to_retrieve: int) -> set:
    """
    Retrieves prime numbers using the Fermat primality test.

    Args:
        range_to_retrieve (int): The upper limit of the range to retrieve prime numbers.

    Returns:
        set: A set of prime numbers within the specified range.
    """
    prime_numbers = set()
    iterations = 5  # Number of iterations for Fermat's test; a larger number yields more accurate results.


    for i in range(2, range_to_retrieve + 1):
        if Fermat_test.is_prime(i, iterations):
            prime_numbers.add(i)

    return prime_numbers


def Rabin_Miller_test_for_range(range_to_retrieve: int) -> set:
    """
    Retrieves prime numbers using the Rabin-Miller primality test.

    Args:
        range_to_retrieve (int): The upper limit of the range to retrieve prime numbers.

    Returns:
        set: A set of prime numbers within the specified range.
    """
    prime_numbers = set()
    iterations = 5  # Number of iterations for Rabin-Miller test,  bigger number more accurate result you get    iterations = 5  # Number of iterations for Rabin-Miller test; a larger number yields more accurate results.

    for i in range(2, range_to_retrieve + 1):
        if Rabin_Miller_test.is_prime(i, iterations):
            prime_numbers.add(i)

    return prime_numbers

def measure_time(function_to_measure: Callable[[int], set], range_to_retrieve: int) -> float:
    """
    Measures the time taken by a function to execute.

    Args:
        function_to_measure (Callable[[int], set]): The function to measure.
        range_to_retrieve (int): The range to pass to the function.

    Returns:
        float: The time taken for the function to execute in seconds.
    """
    start = time.time()
    function_to_measure(range_to_retrieve)
    end = time.time()
    return end - start

def get_percent_of_prime_set_correctness(correct_prime_set: Set[int], prime_set_to_compare: Set[int]) -> float:
    """
    Calculates the percentage of correct prime numbers in the compared prime sets.

    Args:
        correct_prime_set (set): Set of correct prime numbers.
        prime_set_to_compare (set): Set of prime numbers to compare against correct set.

    Returns:
        float: Percentage of correct prime numbers in prime_set_to_compare.
    """
    odd_results = prime_set_to_compare.difference(correct_prime_set)
    total_odd = len(odd_results)
    total_primes = len(correct_prime_set)
    percent_correct =100- (total_odd / total_primes) * 100 if total_primes > 0 else 0
    return percent_correct


def main():
    range_to_retrieve = int(input("Input range to retrieve all prime numbers in this range: "))

    # Measure simple prime retrieval algorithm
    print(f"\nMeasuring simple prime retrieval algorithm with range {range_to_retrieve}")
    time_simple_prime_set = measure_time(simple_prime_retrieval, range_to_retrieve)
    print(f"It took {time_simple_prime_set} seconds")

    # Measure Sieve of Eratosthenes algorithm
    print(f"\nMeasuring Sieve of Eratosthenes algorithm with range {range_to_retrieve}")
    time_Eratosthenes_prime_set = measure_time(sieve_of_Eratosthenes, range_to_retrieve)
    print(f"It took {time_Eratosthenes_prime_set} seconds")

    # Compare times
    if time_simple_prime_set >= time_Eratosthenes_prime_set:
        print('\nSieve of Eratosthenes algorithm is faster than the simple algorithm with this range')
    else:
        print('\nSimple algorithm is faster than the Sieve of Eratosthenes algorithm with this range')

    # Get prime sets from simple algorithm and Sieve of Eratosthenes
    result_simple_prime_set = simple_prime_retrieval(range_to_retrieve)
    result_Eratosthenes_prime_set = sieve_of_Eratosthenes(range_to_retrieve)

    # Compare results
    if result_simple_prime_set == result_Eratosthenes_prime_set:
        print('\nResults are equal')
    else:
        print(f'\nThere was some inconsistency, difference in results is {result_Eratosthenes_prime_set.difference(result_simple_prime_set)}')
    
    
    
    # Here I also added algorithms that use some prime tests I'm familiar with. It was interesting for me to see their efficiency.
    # These algorithms are not used to retrieve all primes in a range, but rather to test if a number is prime (often used in cryptography for very large numbers).
    # So I added these tests for algorithms that iterate through all numbers in a given range.
    # You can uncomment further code below and check it



    # # Measure Fermat test algorithm
    # print(f"\nMeasuring algorithm using Fermat test with range {range_to_retrieve}")
    # time_Fermat_prime_set = measure_time(Fermat_test_for_range, range_to_retrieve)
    # print(f"It took {time_Fermat_prime_set} seconds")

    # # Get prime set from Fermat test
    # result_Fermat_prime_set = Fermat_test_for_range(range_to_retrieve)

   
    # # Compare results of simple prime retrieval algorithm and Fermat test algorithm
    # if result_simple_prime_set == result_Fermat_prime_set:
    #     print('\nResult is correct')
    # else:
    #     print(f'\nThere was some inconsistency, difference in results is {result_Fermat_prime_set.difference(result_simple_prime_set)}')
    
    # print(f"Percentage of correct results using Fermat test: {get_percent_of_prime_set_correctness(result_simple_prime_set,result_Fermat_prime_set):.2f}%")
    
    
    # # Measure Rabin-Miller test algorithm
    # print(f"\nMeasuring algorithm using Rabin-Miller test with range {range_to_retrieve}")
    # time_rabin_miller_prime_set = measure_time(Rabin_Miller_test_for_range, range_to_retrieve)
    # print(f"It took {time_rabin_miller_prime_set} seconds")
    
    # # Get prime set from Rabin-Miller  test
    # result_Rabin_Miller_prime_set = Rabin_Miller_test_for_range(range_to_retrieve)
    
    
    # # Compare results of simple prime retrieval algorithm and Rabin-Miller test algorithm
    # if result_simple_prime_set == result_Rabin_Miller_prime_set:
    #     print('\nResult is correct')
    # else:
    #     print(f'\nThere was some inconsistency, difference in results is {result_Rabin_Miller_prime_set.difference(result_simple_prime_set)}')
    
    # print(f"Percentage of correct results using Rabin-Miller test: {get_percent_of_prime_set_correctness(result_simple_prime_set,result_Rabin_Miller_prime_set):.2f}%")
    
    # # Compare times
    # speeds=[(time_simple_prime_set,"Simple prime retrieval"),(time_Eratosthenes_prime_set,"Sieve of Eratosthenes"),(time_Fermat_prime_set,"Fermat test"),(time_rabin_miller_prime_set,"Rabin-Miller test")]
    # speeds.sort(key=lambda x: x[0])
    # print("\nSpeed comparison rank:")
    # for speed in speeds:
    #     print(f"{speed[1]}: {speed[0]} seconds")
   

    # if time_simple_prime_set <= time_Eratosthenes_prime_set and time_simple_prime_set <= time_Fermat_prime_set and time_simple_prime_set <= time_rabin_miller_prime_set:
    #     print('Simple prime retrieval is the fastest algorithm with this range.')
    # elif time_Eratosthenes_prime_set <= time_simple_prime_set and time_Eratosthenes_prime_set <= time_Fermat_prime_set and time_Eratosthenes_prime_set <= time_rabin_miller_prime_set:
    #     print('Sieve of Eratosthenes is the fastest algorithm with this range.')
    # elif time_Fermat_prime_set <= time_simple_prime_set and time_Fermat_prime_set <= time_Eratosthenes_prime_set and time_Fermat_prime_set <= time_rabin_miller_prime_set:
    #     print('Fermat test is the fastest algorithm with this range.')
    # else:
    #     print('Rabin-Miller test is the fastest algorithm with this range.')

if __name__ == "__main__":
    main()
