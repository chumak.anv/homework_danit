import random


def m_power(x: int, e: int, m: int) -> int:
    """
    Calculates (x^e) % m using iterative method to handle large exponents efficiently.

    Args:
        x (int): The base number.
        e (int): The exponent.
        m (int): The modulus.

    Returns:
        int: The result of (x^e) % m.
    """
    res = 1
    while e > 0:
        if (e % 2) == 1:
            res = (res * x) % m
            e -= 1
        else:
            x = (x * x) % m
            e //= 2
    return res

def is_prime(n: int, k: int) -> bool:
    """
    Uses Fermat's primality test to check if a number is prime.

    Args:
        n (int): The number to check for primality.
        k (int): The number of iterations for the test.

    Returns:
        bool: True if the number is likely prime, False otherwise.
    """
    if n % 2 == 0 and n != 2:  # Even numbers other than 2 are not prime
        return False

    if n <= 3: # 2 and 3 are prime numbers
        return True
    
  # Perform Fermat's test k times
    for _ in range(k):
        a = random.randint(2, n - 2) # Random integer in range [2, n-2]
        if m_power(a, n - 1, n) != 1: # If a^(n-1) % n is not 1, n is not prime
            return False   # Likely prime if it passes all tests

    return True
