# Question Handler API

This FastAPI application provides a simple question and answer service. It includes endpoints to get a random question and check if an answer is correct. The questions are loaded from a JSON file.

## Features

- Retrieve a random question
- Check the correctness of an answer
- Optionally reveal the correct answer if the user's answer is incorrect

## Installation

1. **Clone the repository:**

   ```bash
   git clone git@gitlab.com:chumaana/homework_danit.git
   cd homework_danit/dsci_hw_1
   ```

2. **Create a virtual environment (recommended):**

   ```bash
   python -m venv venv
   source venv/bin/activate  # On Windows use `venv\Scripts\activate`
   ```

3. **Install the dependencies:**

   ```bash
   pip install -r requirements.txt
   ```

## Configuration

Ensure you have a `questions.json` file in the root directory of your project. The file should follow this format:

```json
{
    "questions": [
        {
            "id": 1,
            "question": "What is the capital city of Ukraine?",
            "content": ["Kyiv", "Lviv", "Odesa", "Kharkiv"],
            "correct": 0
        },
        ...
    ]
}

```

### Running the Application

To start the FastAPI server, use the following command:

```bash
uvicorn main:app --reload
```

Replace main with the name of your Python file if it’s different.

The server will run on http://127.0.0.1:8000.

# API Endpoints

## Get Random Question

### GET /play

Returns a random question from the list.

Response:

```json
{
  "id": 1,
  "question": "What is the capital city of Ukraine?",
  "options": {
    "1": "Kyiv",
    "2": "Lviv",
    "3": "Odesa",
    "4": "Kharkiv"
  }
}
```

## Check Answer

### POST /questions/{question_id}/answer

Checks if the selected option is correct.

### Query Parameters:

- selected_option: Integer (1-based index of the selected option)
- reveal_answer: Boolean (optional; if True, reveal the correct answer if the user's answer is incorrect)

Response:

```json
{
  "correct": true,
  "correct_answer": "Kyiv" // Included only if reveal_answer is True and the answer is incorrect
}
```

Errors:

- 404: Question not found
- 400: Invalid option selected
