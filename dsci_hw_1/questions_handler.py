import json
import random
from typing import Dict, List, Union, Optional


class QuestionHandler:
    def __init__(self) -> None:
        """
        Initializes the QuestionHandler by loading questions from a JSON file
        and creating a dictionary for quick access to questions by their ID.
        """
        self.questions = self.get_questions()
        self.question_dict = self.create_question_dict(self.questions)

    @staticmethod
    def get_questions() -> Dict[str, List[Dict[str, Union[int, str, List[str]]]]]:
        """
        Loads the questions from a JSON file.

        Returns:
            A dictionary containing the list of questions.

        Raises:
            FileNotFoundError: If the JSON file is not found.
            json.JSONDecodeError: If the JSON file is not properly formatted.
        """
        try:
            with open("questions.json", "r") as f:
                return json.load(f)
        except FileNotFoundError as e:
            print(f"Error: {e}")
            raise
        except json.JSONDecodeError as e:
            print(f"Error decoding JSON: {e}")
            raise

    def create_question_dict(
        self, questions_data: Dict[str, List[Dict[str, Union[int, str, List[str]]]]]
    ) -> Dict[int, Dict[str, Union[int, str, List[str]]]]:
        """
        Creates a dictionary of questions with the question ID as the key.

        Args:
            questions_data: A dictionary containing the list of questions.

        Returns:
            A dictionary where each key is a question ID and the value is
            the question data.
        """
        question_dict = {}
        for question in questions_data.get("questions", []):
            question_dict[question["id"]] = question
        return question_dict

    def check_answer(self, question_id: int, selected_option: int) -> Optional[bool]:
        """
        Checks if the selected answer is correct for a given question ID.

        Args:
            question_id: The ID of the question to check.
            selected_option: The index of the selected answer (1-based index).

        Returns:
            True if the selected answer is correct, False if incorrect, and
            None if the question ID is not found.
        """
        question = self.question_dict.get(question_id)
        if question:
            correct_answer_index = question["correct"]
            return selected_option == correct_answer_index + 1
        else:
            print(f"Error: Question with ID {question_id} not found.")
            return None

    def get_random_question(
        self,
    ) -> Optional[Dict[str, Union[int, str, Dict[int, str]]]]:
        """
        Selects a random question from the list.

        Returns:
            A dictionary with the question ID, the question text, and a
            dictionary of answer options. Returns None if the question
            dictionary is empty.
        """
        if not self.question_dict:
            print("Error: No questions available.")
            return None

        random_id = random.randint(1, len(self.question_dict))
        question = self.question_dict.get(random_id)
        if question:
            return self.format_question(question)
        else:
            print(f"Error: Question with ID {random_id} not found.")
            return None

    @staticmethod
    def format_question(
        question: Dict[str, Union[int, str, List[str]]]
    ) -> Dict[str, Union[int, str, Dict[int, str]]]:
        """
        Formats a question for display.

        Args:
            question: A dictionary containing the question data.

        Returns:
            A dictionary with the question ID, the question text, and a
            dictionary of answer options.
        """
        return {
            "id": question["id"],
            "question": question["question"],
            "options": {
                idx + 1: answer
                for idx, answer in enumerate(question.get("content", []))
            },
        }
