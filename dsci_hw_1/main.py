from fastapi import FastAPI, HTTPException, Query
from typing import Dict, Union, Optional
from questions_handler import QuestionHandler

app = FastAPI()
question_handler = QuestionHandler()


@app.get("/play")
async def get_random_question() -> Optional[Dict[str, Union[int, str, Dict[int, str]]]]:
    """
    Endpoint to fetch a random question.

    Returns:
        A dictionary with the question ID, question text, and answer options.
        Returns None if no questions are available.

    Raises:
        HTTPException: If no questions are available.
    """
    question = question_handler.get_random_question()
    if not question:
        raise HTTPException(status_code=404, detail="No questions available")
    return question


@app.post("/questions/{question_id}/answer")
async def check_answer(
    question_id: int,
    selected_option: int = Query(..., ge=1, description="1-based index of the option"),
    reveal_answer: bool = False,
) -> Dict[str, Union[bool, str]]:
    """
    Check if the selected answer is correct.

    Args:
        question_id: The ID of the question to check.
        selected_option: The index of the selected answer (1-based).
        reveal_answer: Whether to reveal the correct answer if incorrect.

    Returns:
        A dictionary indicating if the answer is correct, and optionally,
        the correct answer if reveal_answer is True.

    Raises:
        HTTPException: If the question is not found or the option is invalid.
    """
    question = question_handler.question_dict.get(question_id)
    if not question:
        raise HTTPException(status_code=404, detail="Question not found")

    if selected_option < 1 or selected_option > len(question["content"]):
        raise HTTPException(status_code=400, detail="Invalid option selected")

    result = question_handler.check_answer(question_id, selected_option)
    if result is None:
        raise HTTPException(status_code=404, detail="Question not found")

    response = {"correct": result}

    if not result and reveal_answer:
        response["correct_answer"] = question["content"][question["correct"]]

    return response


if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="127.0.0.1", port=8000)
