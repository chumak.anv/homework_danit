

import datetime
import json
from typing import Union

import requests

class CurrencyHandler:
    
    def __init__(self) -> None:
        """Initialize the CurrencyHandler by fetching currency data and setting up currency codes."""
        try:
            response = requests.get("https://api.monobank.ua/bank/currency")
            response.raise_for_status()
        except requests.RequestException as e:
            print(f"An error occurred while fetching currency data: {e}")
            raise SystemExit()

        self.currency_data = response.json()
        self.currency_codes = self.get_currency_codes()
        self.update_date = datetime.datetime.now()
       
    def __str__(self) -> str:
        """Return a string representation of the base currency rates."""
        info_currency = (f"1 EUR = {self.get_uah_rate('EUR', 'UAH')} UAH\n"
                         f"1 USD = {self.get_uah_rate('USD', 'UAH')} UAH\n"
                         f"1 USD = {self.get_currency_rate('USD', 'EUR')} EUR\n")
        date_info = f'Information is actual for {self.update_date.strftime("%b %d, %Y")}'
        return f'\nBase currency rate info:\n{info_currency}{date_info}'
      
    def get_currency_rate(self, currency_from: str, currency_to: str) -> Union[float, None]:
        """Get the exchange rate between two currencies.

        Args:
            currency_from (str): The source currency code.
            currency_to (str): The target currency code.

        Returns:
            Union[float, None]: The exchange rate or None if not available.
        """
        rate_from, rate_to = None, None

        if currency_from not in self.currency_codes or currency_to not in self.currency_codes:
            return None
        
        if currency_from == 'UAH' or currency_to == 'UAH':
            return self.get_uah_rate(currency_from, currency_to)
        
        for rate in self.currency_data:
            if currency_from != 'EUR' and currency_from != 'USD':
                if int(self.currency_codes[currency_from]['number']) == rate['currencyCodeA']:                    
                    rate_from = rate['rateCross']
            if currency_to != 'EUR' and currency_to != 'USD':
                if int(self.currency_codes[currency_to]['number']) == rate['currencyCodeA']:
                    rate_to = rate['rateCross']
                    
            if (currency_from == 'EUR' and currency_to == 'USD') or (currency_to == 'EUR' and currency_from == 'USD'):
                if currency_from == 'EUR' and currency_to == 'USD' and int(self.currency_codes['EUR']['number'])  == rate['currencyCodeA'] and int(self.currency_codes['USD']['number'])  == rate['currencyCodeB']:
                    return rate['rateSell']
                elif currency_to == 'EUR' and currency_from == 'USD' and int(self.currency_codes['EUR']['number'])  == rate['currencyCodeA'] and int(self.currency_codes['USD']['number'])  == rate['currencyCodeB']:
                    return 1 / rate['rateSell']
            
            if currency_from == 'EUR' or currency_from == 'USD':
                if int(self.currency_codes[currency_from]['number']) == rate['currencyCodeA'] and int(self.currency_codes['UAH']['number']) == rate['currencyCodeB']:
                    rate_from = rate['rateSell']
            elif currency_to == 'EUR' or currency_to == 'USD':
                if int(self.currency_codes[currency_to]['number']) == rate['currencyCodeA'] and int(self.currency_codes['UAH']['number'])  == rate['currencyCodeB']:
                    rate_to = rate['rateSell']
        
        return rate_from / rate_to if rate_from and rate_to else None

    def get_uah_rate(self, currency_from: str, currency_to: str) -> Union[float, None]:
        """Get the exchange rate between UAH and another currency.

        Args:
            currency_from (str): The source currency code.
            currency_to (str): The target currency code.

        Returns:
            Union[float, None]: The exchange rate or None if not available.
        """
        for rate in self.currency_data:
            if currency_to == 'UAH' and currency_from not in ['EUR', 'USD']:
                if int(self.currency_codes[currency_from]['number']) == rate['currencyCodeA']:
                    return rate['rateCross']
            elif currency_from == 'UAH' and currency_to not in ['EUR', 'USD']:
                if int(self.currency_codes[currency_to]['number']) == rate['currencyCodeA']:
                    return 1 / rate['rateCross']
            elif currency_to == 'UAH' and currency_from in ['EUR', 'USD']:
                if int(self.currency_codes[currency_from]['number']) == rate['currencyCodeA']:
                    return rate['rateSell']
            elif currency_from == 'UAH' and currency_to in ['EUR', 'USD']:
                if int(self.currency_codes[currency_to]['number']) == rate['currencyCodeA']:
                    return 1 / rate['rateSell']
        return None

    def convert_currency(self, amount: int, currency_from: str, currency_to: str) -> Union[float, None]:
        """Convert an amount from one currency to another.

        Args:
            amount (int): The amount to convert.
            currency_from (str): The source currency code.
            currency_to (str): The target currency code.

        Returns:
            Union[float, None]: The converted amount or None if conversion is not possible.
        """
        rate = self.get_currency_rate(currency_from, currency_to)
        print(rate)
        return amount * rate if rate else None

    def get_currency_rate_text(self, currency_from: str, currency_to: str, amount: int = 1) -> str:
        """Returns the conversion rate and converted amount.

        Args:
            currency_from (str): The source currency code.
            currency_to (str): The target currency code.
            amount (int, optional): The amount to convert. Defaults to 1.
            
        Returns:
            str: A formatted string with the conversion result and date, or an error message.
        
        """
        converted_amount = self.convert_currency(amount, currency_from, currency_to)
        if converted_amount:
            final_text=f"\n{amount} {currency_from} = {converted_amount} {currency_to}"
            final_text+=f'\nInformation is actual for {self.update_date.strftime("%b %d, %Y")}'
        else:
           final_text='\nSomething went wrong during conversion'
        return final_text    

    @staticmethod
    def get_currency_codes() -> dict:
        """Load and return currency codes from a JSON file.

        Returns:
            dict: A dictionary of currency codes.
        """
        try:
            with open('./currency-codes.json', 'r') as file:
                data = json.load(file)
            return data
        except Exception as e:
            print(f"An error occurred while loading currency codes: {e}")
            raise SystemExit()


def print_currency_info(amount: int, currency_from: str, currency_to: str) -> None:
    """Print currency information by converting an amount from one currency to another.

    Args:
        amount (int): The amount to convert.
        currency_from (str): The source currency code.
        currency_to (str): The target currency code.
    """
    currency_converter = CurrencyHandler()
    print(currency_converter)
    currency_rate=currency_converter.get_currency_rate_text(currency_from=currency_from, currency_to=currency_to, amount=amount)
    print(currency_rate)
    
def main() -> None:
    
    currency_from = input('Enter currency code from which you want to convert: ').upper().strip()
    currency_to = input('Enter currency code to which you want to convert: ').upper().strip()
    amount_to_convert = int(input('Enter amount you want to convert: '))
    print_currency_info(amount_to_convert, currency_from, currency_to)

if __name__ == "__main__":
    main()
