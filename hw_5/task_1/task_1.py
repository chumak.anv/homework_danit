from pprint import pprint
from typing import Dict, List


def process_data(data: str) -> List[List[str]]:
    """
    Process order data.

    Args:
        data (str): Input data as a string.

    Returns:
        list: List of orders, where each order is represented as a list of products.
    """
    data = data.strip().split('\n')
    orders_list = [order.split('@@@') for order in data if order]
    return orders_list

def calculate_quantity(orders_list: List[List[str]]) -> Dict[str, int]:
    """
    Calculate the quantity of each product across all orders.

    Args:
        orders_list (list): List of orders, where each order is represented as a list of products.

    Returns:
        dict: Dictionary with products as keys and their quantities as values.
    """
    quantity_of_products= {}
    for order in orders_list:
        for product in order:
            if product in quantity_of_products:
                quantity_of_products[f'{product}']+=1
            else:
                quantity_of_products[f'{product}']=1
                
    return quantity_of_products           
                     

def main():
    with open("orders.txt", "r") as file:
        orders = file.read()
    
    # List of orders that can be used for further associative rule mining
    data_to_analyze = process_data(orders)
    
    
    # Print of all orders
    # for i, order in enumerate(data_to_analyze):
    #     print(f"Order {i + 1}:")
    #     for j,product in enumerate(order):
    #         print(f"{j+1}) {product}")
            
    quantity_of_products_in_orders=calculate_quantity(data_to_analyze) 
    pprint(quantity_of_products_in_orders)       

if __name__ == "__main__":
    main()
