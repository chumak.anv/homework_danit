# Prime Range Retrieval Analysis

This document presents the performance analysis of different methods for retrieving prime numbers within specified ranges.
The methods tested are single-threaded, multi-threaded, and asynchronous, with results compared to understand the impact of
concurrency and parallel execution.

### Test Setup

Three test cases were used, spanning small, medium, and large ranges:

1. Small range: 1 to 1,000
2. Medium range: 200,000 to 500,000
3. Large range: 150,000 to 10,010,000

Each test case measures execution time for the single-threaded, multi-threaded, and asynchronous methods. Results are compared
to evaluate their relative performance.

## Test Results (pytest -s)

```
Single-threaded execution for range 1-1000: 0.0004 seconds
Multi-threaded execution for range 1-1000: 0.0011 seconds
Single-threaded execution for range 200000-500000: 0.1854 seconds
Multi-threaded execution for range 200000-500000: 0.1656 seconds
Single-threaded execution for range 150000-10010000: 17.9903 seconds
Multi-threaded execution for range 150000-10010000: 17.6061 seconds
Async execution for range 1-1000: 0.0008 seconds
Async execution for range 200000-500000: 0.1672 seconds
Async execution for range 150000-10010000: 17.4566 seconds
```

## Analysis of Results

- **Single-threaded performance**: Predictably, single-threaded execution takes the longest time for larger ranges due to its
  sequential nature.
- **Multi-threaded performance**: The multi-threaded approach offers a slight improvement over the single-threaded method,
  especially for larger ranges. However, Python's Global Interpreter Lock (GIL) limits the effectiveness of multi-threading
  for CPU-bound tasks like prime number calculation, as only one thread can execute Python bytecode at a time in the same
  process.

- **Asynchronous performance**: The asynchronous method achieves comparable performance to the multi-threaded approach.
  However, for CPU-bound tasks, asynchronous programming does not offer a substantial improvement. This is because asynchronous
  functions are primarily beneficial for I/O-bound tasks, while CPU-bound tasks still encounter the GIL's limitations.

### The Effect of Python's GIL

The Global Interpreter Lock (GIL) is a mechanism in Python that prevents multiple native threads from executing Python bytecodes
simultaneously in a single process. This design simplifies memory management but restricts true parallelism for CPU-bound tasks.
As a result:

- **Multi-threaded and async approaches do not scale well**: Due to the GIL, only one thread can execute Python bytecode at a
  time, so multi-threading and asynchronous execution don't fully leverage multiple CPU cores for CPU-bound tasks.
- **Why results are close**: Both the multi-threaded and asynchronous approaches are close in performance to the single-threaded
  approach for larger ranges, as the GIL prevents them from achieving true parallel execution.

### Conclusion

For CPU-bound tasks in Python, such as finding prime numbers, the GIL limits the effectiveness of multi-threading and asynchronous
programming. While multi-threading offers a minor performance improvement due to concurrent execution on some parts, its impact
is limited for CPU-intensive operations. To fully leverage multi-core processors for CPU-bound tasks, alternatives like
multiprocessing (which creates separate processes and bypasses the GIL) or using a language without a GIL (such as C++) may be
more effective.
