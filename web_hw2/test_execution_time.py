import time
import pytest
from typing import Tuple, List
from comparison_prime_retrieval_methods import (
    find_primes_single_thread,
    find_primes_multi_thread,
    find_primes_async,
)


@pytest.fixture(params=[(1, 1000), (200000, 500_000), (150_000, 10_010_000)])
def range_params(request) -> Tuple[int, int]:
    """
    Fixture providing a set of range parameters (start, end) for prime-finding tests.

    Args:
        request: The fixture request object to access parameterized values.

    Returns:
        tuple: A start and end integer defining the range.
    """
    return request.param


def test_execution_time_sync(range_params: Tuple[int, int]) -> None:
    """
    Tests and compares the execution time of single-threaded and multi-threaded
    prime-finding functions.

    Args:
        range_params (Tuple[int, int]): Tuple containing the start and end range for testing.
    """
    start, end = range_params

    start_time_single = time.time()
    result_single_thread: List[int] = find_primes_single_thread(start, end)
    end_time_single = time.time()
    execution_time_single = end_time_single - start_time_single
    print(
        f"Single-threaded execution for range {start}-{end}: {execution_time_single:.4f} seconds"
    )

    start_time_multi = time.time()
    result_multi_thread: List[int] = find_primes_multi_thread(start, end)
    end_time_multi = time.time()
    execution_time_multi = end_time_multi - start_time_multi
    print(
        f"Multi-threaded execution for range {start}-{end}: {execution_time_multi:.4f} seconds"
    )

    assert result_single_thread == result_multi_thread, (
        f"Results differ for range {start}-{end}. "
        f"Single-threaded: {result_single_thread[:10]}..., "
        f"Multi-threaded: {result_multi_thread[:10]}..."
    )


@pytest.mark.asyncio
async def test_execution_time_async(range_params: Tuple[int, int]) -> None:
    """
    Tests and compares the execution time of the async prime-finding function
    with the single-threaded result.

    Args:
        range_params (Tuple[int, int]): Tuple containing the start and end range for testing.
    """
    start, end = range_params

    start_time_async = time.time()
    result_async: List[int] = await find_primes_async(start, end)
    end_time_async = time.time()
    execution_time_async = end_time_async - start_time_async
    print(
        f"Async execution for range {start}-{end}: {execution_time_async:.4f} seconds"
    )

    result_single_thread: List[int] = find_primes_single_thread(start, end)
    assert result_async == result_single_thread, (
        f"Results differ for range {start}-{end}. "
        f"Async: {result_async[:10]}..., "
        f"Single-threaded: {result_single_thread[:10]}..."
    )
