import concurrent.futures
import asyncio
from typing import List


def is_prime(n: int) -> bool:
    """
    Determines if a number is prime.

    Args:
        n (int): The number to check for primality.

    Returns:
        bool: True if n is prime, False otherwise.
    """
    if n <= 1:
        return False
    if n <= 3:
        return True
    if n % 2 == 0 or n % 3 == 0:
        return False
    i = 5
    while i * i <= n:
        if n % i == 0 or n % (i + 2) == 0:
            return False
        i += 6
    return True


def find_primes_single_thread(start: int, end: int) -> List[int]:
    """
    Finds all prime numbers within a range using a single thread.

    Args:
        start (int): The start of the range.
        end (int): The end of the range.

    Returns:
        List[int]: A sorted list of prime numbers within the specified range.
    """
    prime_numbers = set()
    for candidate in range(start, end + 1):
        if is_prime(candidate):
            prime_numbers.add(candidate)
    return sorted(prime_numbers)


def find_primes_multi_thread(start: int, end: int) -> List[int]:
    """
    Finds all prime numbers within a range using multiple threads.

    Args:
        start (int): The start of the range.
        end (int): The end of the range.

    Returns:
        List[int]: A list of prime numbers within the specified range.
    """
    prime_numbers = []
    mid = (start + end) // 2
    ranges = [(start, mid), (mid + 1, end)]

    with concurrent.futures.ThreadPoolExecutor(max_workers=2) as executor:
        results = executor.map(find_primes_single_thread, *ranges)

        for result in results:
            prime_numbers.extend(result)

    return prime_numbers


async def find_primes_single_async(start: int, end: int) -> List[int]:
    """
    Asynchronously finds all prime numbers within a range by running
    a CPU-bound task in a separate thread.

    Args:
        start (int): The start of the range.
        end (int): The end of the range.

    Returns:
        List[int]: A sorted list of prime numbers within the specified range.
    """
    return await asyncio.to_thread(find_primes_single_thread, start, end)


async def find_primes_async(start: int, end: int) -> List[int]:
    """
    Finds all prime numbers within a range asynchronously, dividing
    the range into two parts and processing each part concurrently.

    Args:
        start (int): The start of the range.
        end (int): The end of the range.

    Returns:
        List[int]: A list of prime numbers within the specified range.
    """
    prime_numbers = []
    mid = (start + end) // 2
    ranges = [(start, mid), (mid + 1, end)]

    results = await asyncio.gather(
        find_primes_single_async(*ranges[0]),
        find_primes_single_async(*ranges[1]),
    )

    for result in results:
        prime_numbers.extend(result)

    return prime_numbers
